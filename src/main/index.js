import { app, BrowserWindow, Tray, Menu, nativeImage } from 'electron'
import path from 'path'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
let tray = null;

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1024,
    webPreferences: {
      webSecurity: false
    }
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('close', e => {
    e.preventDefault();

    if (!tray) {
      _closeApp();
    } else {
      _showHideWindow();
    }
  })
}

function _showHideWindow(e) {
  if (!mainWindow) return;

  mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
}

function _closeApp() {
  if (process.platform !== 'darwin') {
    app.quit()
  }

  mainWindow && mainWindow.destroy()
  tray && tray.destroy();

  tray = null
  mainWindow = null
}

function createTray () {
  // tray = new Tray( nativeImage.createFromPath('static/trayicon.png') );
  tray = new Tray( nativeImage.createFromDataURL(`data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4RjVBMTk3OTc3MDQxMUU2QkI4ODg3MUM2RDc1QThGNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4RjVBMTk3QTc3MDQxMUU2QkI4ODg3MUM2RDc1QThGNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjhGNUExOTc3NzcwNDExRTZCQjg4ODcxQzZENzVBOEY2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjhGNUExOTc4NzcwNDExRTZCQjg4ODcxQzZENzVBOEY2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+KGUPrgAAAAxQTFRFMLdM/v/+////L7dLFvYZ3QAAAEFJREFUeNpiYEYDDEQLMDGhCjAxwUQY4HyoCAOCDxFhQOKDRYACjCAmI4REqICbi1OAgQFNANlQZkY4IMpzAAEGAG1cApeibgesAAAAAElFTkSuQmCC`) );

  const contextMenu = Menu.buildFromTemplate([
    {label: 'Показать/Скрыть', click: _showHideWindow},
    // {label: 'Перейти на сайт'},
    // {label: 'Обновить сейчас'},
    {type: 'separator'},
    {label: 'Выход', click: _closeApp}
  ]);

  // tray.on('click', _showHideWindow)

  tray.setTitle('Sibdom Updater')
  tray.setToolTip('Sibdom Updater');
  tray.setContextMenu(contextMenu);
}

app.on('ready', () => {
  createWindow();
  createTray();
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
