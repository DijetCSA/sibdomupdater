import categoriesService from '../../services/Categories';
import authService from '../../services/Auth';
import mailService from '../../services/Mail';
import config from '../../../configs/config.json';

const state = {
  loading: false,
  categoriesList: []
}

const mutations = {
  LOAD_CATEGORIES_LIST (state, res) {
    state.loading = true;
    // TODO: Сделать парсинг и погрузку с сайта
    state.categoriesList = require('../../../configs/categoriesList.json').map(category =>
     Object.assign({
         updating: false,
         progressLots: 0,
         totalLots: 0,
         errorMsg: null
      }, category)
    );
    state.loading = false;
  },
  CLEAR_CATEGORY_STATE (state, category) {
    category.progressLots = 0;
    category.totalLots = 0;
    category.errorMsg = null;
  },
  UPDATE_CATEGORY_STATE (state, data) {
    data.category.updating = !data.state.finished;
    data.category.progressLots = data.state.progressLots;
    data.category.totalLots = data.state.totalLots;
    data.category.errorMsg = data.state.errorMsg || null;
  },
  SET_CATEGORY_UPDATING_STATE (state, data) {
    data.category.updating = !!data.updating;
  }
}

const actions = {
  loadCategoriesList ({ commit }) {
    commit('LOAD_CATEGORIES_LIST');
  },
  updateCategory ({ commit, dispatch }, category) {
    function _updateCat() {
      if (category.updating) return Promise.resolve();
      commit('CLEAR_CATEGORY_STATE', category);
      commit('SET_CATEGORY_UPDATING_STATE', {category, updating: true})

      return categoriesService
        .updateLots(category.url, state => commit('UPDATE_CATEGORY_STATE', {category, state}))
    }


    return authService.getAuthState()
      .catch( err => dispatch('login') )
      .catch( err => {
        commit('UPDATE_CATEGORY_STATE', {category, state: {
          finished: true,
          processed_row: 0,
          total_row: 0,
          errorMsg: 'Не удалось выполнить обновление, пользователь не авторизирован!'
        }});
        commit("SET_USER_DATA", null);

        return Promise.reject();
      } )
      .then( res => _updateCat() )
  },
  updateCategories ({ dispatch, commit }, categories=state.categoriesList) {
    let updateCategoriesChain = Promise.resolve();

    for (let i = 0; i < categories.length; i++) {
      updateCategoriesChain = updateCategoriesChain.finally( () => dispatch("updateCategory", categories[i]) )
    }

    return updateCategoriesChain
      .then( result => {
        // Код, который выполняется после удачного завершения цепочки действий
      })
      .catch( error => {
        mailService.sendErrorLatter();
        // Обработка ошибок
      });
  },
  createCategoriesUpdateTask({ dispatch, commit }, {time=config.defaultUpdateTime, timer}) {
    dispatch('updateTimer', {
      newTime: time, timer,
      callback: () => dispatch('updateCategories')
    });
  },
  initCategoriesUpdateTask({ dispatch }) {
    dispatch( 'initTimers', () => dispatch('updateCategories') )
    // dispatch('getTimers').then(timers => {
    //   for (var i = 0; i < timers.length; i++) {
    //     dispatch('updateTimer', {
    //       newTime: time, timer,
    //       callback:
    //     });
    //   }
    // })
  }
}

export default {
  state,
  mutations,
  actions
}
