import authService from '../../services/Auth';
import config from '../../../configs/config.json';

import {CronJob} from 'cron';

import utils from '../../services/utils';

let cronJobs = {};

const state = {
  loading: true,
  authUser: null,
  // updateTime: config.defaultUpdateTime || "00:00",
  timers: config.timers.map(time => ({
    time, cronId: null, editing: false
  })) || []
}

const mutations = {
  SET_USER_DATA (state, res) {
    state.authUser = res;
  },
  SET_LOADING (state, loadingState) {
    state.loading = !!loadingState;
  },
  UPDATE_TIMER (state, {index, time, callback}) {
    if (!time || !/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(time)) {
      alert('Таймер не утановлен. Неверный формат времени.');
      return;
    }

    let timer = state.timers[index];
    timer.time = time;

    let cronId = timer.cronId || utils.makeid();

    if (cronId && cronJobs[cronId]) {
      cronJobs[cronId].stop();
    }

    let hours = Number( time.substr(0,2) );
    let minutes = Number( time.substr(3,2) );

    timer.cronId = cronId;
    cronJobs[cronId] = new CronJob(`00 ${minutes} ${hours} * * *`, callback, null, true);
  },
  SET_TIMER_EDITABLE (storeState, {index, state}) {
    storeState.timers[index].editing = !!state;
  },
  REMOVE_TIMER (state, index) {
    let timer = state.timers[index];

    let cronId = timer.cronId;
    if (cronId && cronJobs[cronId]) {
      cronJobs[cronId].stop();
      delete cronJobs[cronId];
    }

    state.timers.splice(index, 1);
  }
}

const actions = {
  login ({ commit, state }, formData) {
    if (!formData && state.authUser && state.authUser.password) {
      formData = {
        login: state.authUser.login || state.authUser.email,
        password: state.authUser.password
      }
    }

    return authService.login(formData)
      .then(res => {
        commit('SET_USER_DATA', Object.assign(res, formData))
      })
  },
  checkAuth ({ commit }) {
    return authService.getAuthState()
      .then(userData => {
        commit('SET_USER_DATA', userData);
      })
      .catch(err => {
        if (err.status === 401) return commit('SET_USER_DATA', null);
      })
      .finally( () => commit('SET_LOADING', false) );
  },
  updateTimer ({ commit, state }, { timer, newTime, callback }) {
    let index = state.timers.indexOf(timer)

    if (index !== -1) {
      commit('UPDATE_TIMER', { index, time: newTime, callback });
    }
  },
  editTimer ({ commit, state }, timer) {
    let index = state.timers.indexOf(timer)

    if (index === -1) return;

    for (var i = 0; i < state.timers.length; i++) {
      commit('SET_TIMER_EDITABLE', {
        index: i, state: false
      })
    }

    commit('SET_TIMER_EDITABLE', {
      index, state: true
    })
  },
  editTimerOff ({ commit, state }, timer) {
    let index = state.timers.indexOf(timer)

    if (index === -1) return;

    commit('SET_TIMER_EDITABLE', {
      index, state: false
    })
  },
  removeTimer ({ commit, state }, timer) {
    let index = state.timers.indexOf(timer)

    if (index === -1) return;

    commit('REMOVE_TIMER', index)
  },
  initTimers ({ commit, state }, callback) {
    for (var i = 0; i < state.timers.length; i++) {
      commit('UPDATE_TIMER', { index:i, time: state.timers[i].time, callback });
    }
  }
}

export default {
  state,
  mutations,
  actions
}
