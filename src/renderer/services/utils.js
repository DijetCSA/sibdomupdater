export default {
	fetch(url, options) {
		return fetch(url, options)
			.then(resp => resp)
			.catch(err => {
				if ( String(err) === 'TypeError: Failed to fetch') {
					// alert('Проблемы с сетью, обратитесь к системному администратору!')
				}
				return Promise.reject(err);
			})
	},
	buildUrl(url, parameters) {
    let qs = "";
    for (const key in parameters) {
      if (parameters.hasOwnProperty(key)) {
        const value = parameters[key];
        qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
      }
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); //chop off last "&"
      url = url + "?" + qs;
    }

    return url;
	},
	makeid() {
	  let text = "";
	  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (let i = 0; i < 5; i++)
	    text += possible.charAt(Math.floor(Math.random() * possible.length));

	  return text;
	}
};
