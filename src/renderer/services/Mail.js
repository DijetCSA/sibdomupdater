import config from '../../configs/config.json';
import nodemailer from 'nodemailer';

// TODO: Вынести все в конфиг
const transporter = nodemailer.createTransport({
	host: 'smtp.mail.ru',
	port: 465,
	secure: true, // true for 465, false for other ports
	auth: config.supportMailAccount
});

// create reusable transporter object using the default SMTP transport
function _getMailOptionsForUpdateCategoriesError({ emails=config.subscribers.join(',') }={}) {
	return {
		from: `"Sibdom-updater 👻" <${config.supportMailAccount.user}>`, // sender address
		to: emails, // list of receivers
		subject: 'Какие-то проблемы :(', // Subject line
		text: 'Какие-то проблемы :(', // plain text body
		html: `<p>При обновлении категорий, одну или более категорий не удалось обновить.
						Проверьте приложение для более подробной информации и обратитесь к администратору приложения.</p>`
	}
};

export default {
	sendErrorLatter(config={}) {
		return new Promise( (resolve, reject) => {
			transporter.sendMail(_getMailOptionsForUpdateCategoriesError(config), (error, info) => {
				if (error) {
					reject(error);
					return;
				}
				resolve(info);
			});
		});
	}
};
