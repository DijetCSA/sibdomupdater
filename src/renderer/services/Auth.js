import config from '../../configs/config.json';
import cheerio from 'cheerio';
import utils from './utils';

const authService = {
	logout() {

	},
	login({login, password}) {
		if (!login || !password) {
			// alert('Не указан логин или пароль!')
			return Promise.reject();
		}

		let authForm = new FormData();

		authForm.append('cabAuthUsername', login);
		authForm.append('cabAuthPassword', password);
		authForm.append('cabAuthCheck', 1);
		authForm.append('go', 'Войти');

		return utils.fetch(config.authUrl, {
			method:				"POST",
			credentials:	"include",
			body: 				authForm
		})
		.then( response => response.text() )
		.then( bodyText => cheerio.load(bodyText) )
		.then( $ => {
			if ( $('.forum-auth .alert').text().includes('Неверный Email или пароль') ) {

				return Promise.reject({
					status: 401,
					message: 'Неверный Email или пароль'
				});

			}

			return this.getAuthState();
		})
	},
	getAuthState() {
		return utils.fetch(config.checkAuthStateUrl, {
			method:				"GET",
			credentials:	"include"
		})
		.then( response => {
			if (response.redirected) {
				return Promise.reject({
					status: 401,
					message: 'Не удалось авторизироваться. Возможно устарели ссылки, обратитесь к администратору!'
				});
			}

			return response.text();
		})
		.then( bodyText => cheerio.load(bodyText) )
		.then( $ => {
				let username = $('.profile a[href="/cabinet/"]').text();
				let email = $('label[for="email"]').closest('li').find('.content > strong').text();

				return { username, email };
		})
	}
};

export default authService;
