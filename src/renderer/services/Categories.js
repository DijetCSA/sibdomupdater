import config from '../../configs/config.json';
import utils from './utils';

const categoriesService = {
	updateLots(url, cb) {
		return new Promise((resolve, reject) => {
			function _progressUpdate(url, data={}) {
				utils.fetch(utils.buildUrl(url, data), {
					method: "GET",
					credentials: "include",
					headers: new Headers({
						'Host': 'www.sibdom.ru',
						'Connection': 'keep-alive',
						'Accept': 'application/json, text/javascript, */*; q=0.01',
						'X-Requested-With': 'XMLHttpRequest',
						'Referer': url,
						'Accept-Encoding': 'gzip, deflate',
						'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
					})
				})
				.then( res => res.json() )
				.then( data => {
					if (!data || data.error) {
						reject(data.error)
						return;
					}

					let state = {
			      progressLots: data.processed_row,
			      totalLots: data.total_row
			    }

					if (data.count_update_row > 0) {
						state.finished = false;
						cb && cb(state);
						_progressUpdate(url, data);
					} else {
						state.finished = true;
						cb && cb(state);
						resolve()
					}
				})
				.catch(err => {
					if (String(err) === "SyntaxError: Unexpected token < in JSON at position 0") {
						cb && cb({
							finished: true,
							processed_row: 0,
							total_row: 0,
							errorMsg: 'Не удалось выполнить обновление, возможно категория пуста!'
						})
					}

					return reject(err);
				})
			};
			_progressUpdate(url);
		})
	}
}

export default categoriesService;
